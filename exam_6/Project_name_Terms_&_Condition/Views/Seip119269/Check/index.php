<a href="create.php"> Add Check box</a>

<?php
    include_once ('../../../vendor/autoload.php');

    use App\Seip119269\Check\Check; 
    $obj = new Check();
    $data = $obj->index();
    
        if(isset($_SESSION['Message'])&& !empty($_SESSION['Message'])){
            echo $_SESSION['Message'];
            unset($_SESSION['Message']);
        }
?>

<table border="1">
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Checked</th>
        <th colspan="3">Action</th>
    </tr>
    
    <?php if(isset($data)){?>
    <?php foreach ($data as $item){?>
    <tr>
        <td><?php echo $item['id'];?></td>
        <td><?php echo $item['title'];?></td>
        
        <?php if(isset($item['check']) && $item['check'] =="Check"){ ?>
            <td><input type="checkbox" name="check" checked="checked" value="<?php echo $data['check'];?>"></td>
        <?php }else{ ?> 
            <td><input type="checkbox" name="check" value="<?php echo $data['check'];?>"></td>
        <?php }?>
           
        <td><a href="show.php?id=<?php echo $item['id'];?>">View</a></td>
        <td><a href="edit.php?id=<?php echo $item['id'];?>">Edit</a></td>
        <td><a href="delete.php?id=<?php echo $item['id'];?>">Delete</a></td>
    </tr>
    <?php }} ?>
</table>