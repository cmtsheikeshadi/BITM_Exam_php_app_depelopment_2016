<?php
    include_once ('../../../vendor/autoload.php');

    use App\Seip119269\Check\Check; 
    $obj = new Check();
    $data = $obj->prepare($_GET)->show();
?>

<table border="1">
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Checked</th>
    </tr>
    <tr>
        <td><?php echo $data['id'];?></td>
        <td><?php echo $data['title'];?></td>
        <?php if(isset($data['check']) && $data['check'] =="Check"){ ?>
            <td><input type="checkbox" name="check" checked="checked" value="<?php echo $data['check'];?>"></td>
        <?php }else{ ?> 
            <td><input type="checkbox" name="check" value="<?php echo $data['check'];?>"></td>
        <?php }?>
        
    </tr>
</table>